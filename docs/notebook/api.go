package notebook

import "gitlab.com/ethan.reesor/vscode-notebooks/lib/notebook"

type Formatter = notebook.Formatter
type Content = notebook.Content
type MultiFormat = notebook.MultiFormat
type SpewFormat = notebook.SpewFormat
type SQLFormat = notebook.SQLFormat
type JSONFormat = notebook.JSONFormat
type XMLFormat = notebook.XMLFormat
type MarkdownFormat = notebook.MarkdownFormat

func Halt(v interface{})
func Inspect(v ...[]interface{})
func Show(v ...[]interface{})
