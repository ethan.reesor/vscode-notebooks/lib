package vscode

type PromptOptions struct {
	Prompt      string
	Placeholder string
	Secret      bool
}

func Prompt(opts *PromptOptions) (string, bool)
func Cache(key string, get func() string) string
func Uncache(key string)
