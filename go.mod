module gitlab.com/ethan.reesor/vscode-notebooks/lib

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/traefik/yaegi v0.9.21
	gitlab.com/firelizzard/yaegi-dsl v0.0.0-20210918080752-3547d3de8258
	google.golang.org/protobuf v1.27.1
)
