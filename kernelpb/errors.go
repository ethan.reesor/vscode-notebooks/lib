package kernelpb

import (
	"time"

	"google.golang.org/protobuf/types/known/durationpb"
)

func (msg *Eval_Result) AddError(message, stack string) {
	var pstack *string
	if stack != "" {
		pstack = &stack
	}
	msg.Errors = append(msg.Errors, &Error{
		Message: message,
		Stack:   pstack,
	})
}

func (msg *Eval_Result) AddErrorAt(message, stack string, line, col int) {
	var pstack *string
	if stack != "" {
		pstack = &stack
	}
	msg.Errors = append(msg.Errors, &Error{
		Message:  message,
		Stack:    pstack,
		Position: &Error_LineAndColumn{Line: int32(line), Column: int32(col)},
	})
}

func (msg *Eval_Result) SetDuration(d time.Duration) {
	msg.Duration = durationpb.New(d)
}
