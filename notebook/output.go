package notebook

import (
	"database/sql"
	"encoding/json"
	"encoding/xml"
	"fmt"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/sqldata"
)

type Formatter interface {
	Format() ([]Content, error)
}

type Content struct {
	Type  string
	Value []byte
}

func (c Content) Format() ([]Content, error) {
	return []Content{c}, nil
}

type MultiFormat []Formatter

func (o MultiFormat) Format() ([]Content, error) {
	var all []Content
	for _, o := range o {
		o, err := o.Format()
		if err != nil {
			return nil, err
		}
		all = append(all, o...)
	}
	return all, nil
}

type SpewFormat struct {
	Value interface{}
}

func (o SpewFormat) Format() ([]Content, error) {
	return []Content{
		{Type: "text/plain", Value: []byte(spew.Sdump(o.Value))},
	}, nil
}

type SQLFormat struct {
	Rows *sql.Rows
}

func (o SQLFormat) Format() ([]Content, error) {
	cols, rows, err := sqldata.GetRows(o.Rows)
	if err != nil {
		return nil, err
	}

	return []Content{
		{Type: "application/json", Value: sqldata.MarshalJSON(cols, rows)},
		{Type: "text/html", Value: sqldata.MarshalHTML(cols, rows)},
		{Type: "text/csv", Value: sqldata.MarshalCSV(cols, rows)},
	}, nil
}

type JSONFormat struct {
	Value interface{}
}

func (o JSONFormat) Format() ([]Content, error) {
	switch v := o.Value.(type) {
	case string:
		return []Content{
			{Type: "application/json", Value: []byte(v)},
		}, nil
	case []byte:
		return []Content{
			{Type: "application/json", Value: v},
		}, nil
	}

	b, err := json.MarshalIndent(o.Value, "", "  ")
	if err != nil {
		return nil, err
	}
	return []Content{
		{Type: "application/json", Value: b},
	}, nil
}

type XMLFormat struct {
	Value interface{}
}

func (o XMLFormat) Format() ([]Content, error) {
	switch v := o.Value.(type) {
	case string:
		return []Content{
			{Type: "application/xml", Value: []byte(v)},
		}, nil
	case []byte:
		return []Content{
			{Type: "application/xml", Value: v},
		}, nil
	}

	b, err := xml.MarshalIndent(o.Value, "", "  ")
	if err != nil {
		return nil, err
	}
	return []Content{
		{Type: "application/xml", Value: b},
	}, nil
}

type MarkdownFormat struct {
	Language string
	Value    interface{}
}

func (o MarkdownFormat) Format() ([]Content, error) {
	if o.Language == "" {
		return []Content{
			{Type: "text/markdown", Value: []byte(fmt.Sprint(o.Value))},
		}, nil
	}

	var err error
	v := o.Value
	switch o.Language {
	case "json":
		switch v.(type) {
		case string, []byte:
		default:
			v, err = json.MarshalIndent(v, "", "  ")
		}
	case "xml":
		switch v.(type) {
		case string, []byte:
		default:
			v, err = xml.MarshalIndent(v, "", "  ")
		}
	}
	if err != nil {
		return nil, err
	}

	return []Content{
		{Type: "text/markdown", Value: []byte(fmt.Sprintf("```%s\n%s\n```", o.Language, v))},
	}, nil
}
