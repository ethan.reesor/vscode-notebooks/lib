package sqldata

import (
	"bytes"
	"database/sql"
	"database/sql/driver"
	"encoding/base64"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
)

func GetRows(rows *sql.Rows) ([]*sql.ColumnType, [][]interface{}, error) {
	cols, err := rows.ColumnTypes()
	if err != nil {
		return nil, nil, err
	}

	result := [][]interface{}{}

	rvals := make([]reflect.Value, len(cols))
	vals := make([]interface{}, len(cols))
	for rows.Next() {
		for i := range cols {
			typ := cols[i].ScanType()
			if typ == nil {
				typ = reflect.TypeOf((*interface{})(nil)).Elem()
			}
			rvals[i] = reflect.New(typ)
			vals[i] = rvals[i].Interface()
		}

		err := rows.Scan(vals...)
		if err != nil {
			return nil, nil, err
		}

		entry := make([]interface{}, len(cols))
		for i := range cols {
			v := rvals[i].Elem().Interface()
			typ := cols[i].DatabaseTypeName()

			switch v := v.(type) {
			case driver.Valuer:
				u, err := v.Value()
				if err == nil {
					entry[i] = u
				} else {
					entry[i] = v
				}

			case sql.RawBytes:
				if typ == "CHAR" || typ == "VARCHAR" {
					entry[i] = string([]byte(v))
				} else {
					entry[i] = base64.StdEncoding.EncodeToString([]byte(v))
				}

			default:
				entry[i] = v
			}
		}

		result = append(result, entry)
	}

	return cols, result, nil
}

func MarshalJSON(cols []*sql.ColumnType, rows [][]interface{}) []byte {
	buf := new(bytes.Buffer)

	s := make([][]byte, len(cols))
	for i := range cols {
		s[i], _ = json.Marshal(cols[i].Name())
	}

	buf.WriteString("[\n")
	for i, row := range rows {
		if i > 0 {
			buf.WriteString(",\n")
		}
		buf.WriteString("\t{ ")
		for j := range s {
			if j > 0 {
				buf.WriteString(", ")
			}

			buf.Write(s[j])
			buf.WriteString(": ")

			b, err := json.Marshal(row[j])
			if err == nil {
				buf.Write(b)
			} else {
				buf.WriteString(strconv.Quote(err.Error()))
			}
		}
		buf.WriteString("}")
	}
	buf.WriteString("\n]")

	return buf.Bytes()
}

func MarshalCSV(cols []*sql.ColumnType, rows [][]interface{}) []byte {
	buf := new(bytes.Buffer)
	w := csv.NewWriter(buf)

	s := make([]string, len(cols))
	for i := range cols {
		s[i] = cols[i].Name()
	}
	w.Write(s)

	for _, row := range rows {
		for i := range row {
			s[i] = fmt.Sprint(row[i])
		}
		w.Write(s)
	}

	w.Flush()
	return buf.Bytes()
}

func MarshalHTML(cols []*sql.ColumnType, rows [][]interface{}) []byte {
	out := new(bytes.Buffer)
	out.WriteString(`<table><thead><tr>`)

	for _, col := range cols {
		out.WriteString(fmt.Sprintf(`<td>%s</td>`, col.Name()))
	}

	out.WriteString(`</tr></thead><tbody>`)

	for _, row := range rows {
		out.WriteString(`<tr>`)

		for _, v := range row {
			out.WriteString(fmt.Sprintf(`<td>%v</td>`, v))
		}

		out.WriteString(`</tr>`)
	}

	out.WriteString(`</body></table>`)

	return out.Bytes()
}
