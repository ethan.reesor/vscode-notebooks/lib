package yaegi

import (
	"errors"
	"fmt"
	"go/scanner"

	"github.com/traefik/yaegi/interp"
)

type HaltEval struct {
	Value interface{}
}

type ErrorResult interface {
	AddError(message, stack string)
	AddErrorAt(message, stack string, line, col int)
}

func UnfoldError(err error, res ErrorResult) {
	var errPanic interp.Panic
	if errors.As(err, &errPanic) {
		if halt, ok := errPanic.Value.(HaltEval); ok {
			res.AddError(fmt.Sprintf("Halted: %v", halt.Value), "")
			return
		}
		res.AddError(fmt.Sprint(errPanic.Value), string(errPanic.Stack))
		return
	}

	var errList scanner.ErrorList
	if errors.As(err, &errList) {
		for _, err := range errList {
			var errScanner *scanner.Error
			if errors.As(err, &errScanner) {
				res.AddErrorAt(err.Msg, "", errScanner.Pos.Line, errScanner.Pos.Line)
			} else {
				res.AddError(err.Msg, "")
			}
		}
		return
	}

	if errors.Is(err, ErrCanceled) {
		res.AddError("Canceled", "")
		return
	}

	res.AddError(err.Error(), "")
}
