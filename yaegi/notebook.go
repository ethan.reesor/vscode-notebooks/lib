package yaegi

import (
	"database/sql"

	"github.com/traefik/yaegi/interp"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/notebook"
	dsl "gitlab.com/firelizzard/yaegi-dsl"
)

func UseNotebookAPI(s *Session, send func(notebook.Formatter)) error {
	return s.Use(interp.Exports{
		"notebook/notebook": (&notebookDSL{send: send}).symbols(),
	})
}

type notebookDSL struct {
	dsl.DSL `dsl-docs:"notebook,../docs/notebook/api.go"`

	Formatter      notebook.Formatter
	Content        notebook.Content
	MultiFormat    notebook.MultiFormat
	SpewFormat     notebook.SpewFormat
	SQLFormat      notebook.SQLFormat
	JSONFormat     notebook.JSONFormat
	XMLFormat      notebook.XMLFormat
	MarkdownFormat notebook.MarkdownFormat

	send func(notebook.Formatter)
}

func (dsl *notebookDSL) Halt(v interface{}) {
	panic(HaltEval{Value: v})
}

func (dsl *notebookDSL) Inspect(v ...interface{}) {
	for _, v := range v {
		dsl.send(notebook.SpewFormat{Value: v})
	}
}

func (dsl *notebookDSL) Show(v ...interface{}) {
	for _, v := range v {
		if r, ok := v.(*sql.Rows); ok {
			dsl.send(notebook.SQLFormat{Rows: r})
			continue
		}

		if c, ok := v.(notebook.Formatter); ok {
			dsl.send(c)
			continue
		}

		dsl.send(notebook.SpewFormat{Value: v})
	}
}
