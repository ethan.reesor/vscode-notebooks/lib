package yaegi

import (
	"github.com/traefik/yaegi/interp"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/kernelpb"
	dsl "gitlab.com/firelizzard/yaegi-dsl"
)

func UseVSCodeAPI(s *Session, api VSCodeAPI) error {
	return s.Use(interp.Exports{
		"vscode/vscode": (&vscodeDSL{api: api}).symbols(),
	})
}

type VSCodeAPI interface {
	Done() <-chan struct{}

	Prompt(*kernelpb.VSCode_Prompt)
	Prompted() <-chan *kernelpb.VSCode_Prompted

	ReadCache(*kernelpb.VSCode_ReadCache)
	WriteCache(*kernelpb.VSCode_WriteCache)
	Cached() <-chan *kernelpb.VSCode_Cached
}

type vscodeDSL struct {
	dsl.DSL `dsl-docs:"vscode,../docs/vscode/api.go"`

	PromptOptions promptOptions

	api VSCodeAPI
}

type promptOptions struct {
	Prompt      string
	Placeholder string
	Secret      bool
}

func (dsl *vscodeDSL) Prompt(opts *promptOptions) (string, bool) {
	if opts == nil {
		opts = new(promptOptions)
	}

	prompt := new(kernelpb.VSCode_Prompt)
	prompt.Secret = opts.Secret
	if opts.Prompt != "" {
		prompt.Prompt = &opts.Prompt
	}
	if opts.Placeholder != "" {
		prompt.Placeholder = &opts.Placeholder
	}

	dsl.api.Prompt(prompt)

	select {
	case <-dsl.api.Done():
		return "", false
	case resp := <-dsl.api.Prompted():
		if resp.Value == nil {
			return "", false
		}
		return *resp.Value, true
	}
}

func (dsl *vscodeDSL) Cache(key string, get func() string) string {
	dsl.api.ReadCache(&kernelpb.VSCode_ReadCache{
		Key: key,
	})

	var cached *kernelpb.VSCode_Cached
	select {
	case <-dsl.api.Done():
		return ""
	case cached = <-dsl.api.Cached():
	}

	if cached.Value != nil {
		return *cached.Value
	}

	v := get()
	wc := new(kernelpb.VSCode_WriteCache)
	wc.Key = key
	if v != "" {
		wc.Value = &v
	}
	dsl.api.WriteCache(wc)

	select {
	case <-dsl.api.Done():
	case <-dsl.api.Cached():
	}

	return v
}

func (dsl *vscodeDSL) Uncache(key string) {
	dsl.api.WriteCache(&kernelpb.VSCode_WriteCache{Key: key})

	select {
	case <-dsl.api.Done():
	case <-dsl.api.Cached():
	}
}
